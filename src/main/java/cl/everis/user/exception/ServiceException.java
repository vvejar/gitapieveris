package cl.everis.user.exception;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param msg
	 */
	public ServiceException(String msg){
		super(msg);		
	}
	
}
