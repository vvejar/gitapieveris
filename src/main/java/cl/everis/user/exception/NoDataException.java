package cl.everis.user.exception;

public class NoDataException extends Exception {

	
  private static final long serialVersionUID = -6585474871563350948L;

  /**
   * Constructor
   * @param msg
   */
  public NoDataException(String msg) {
		super(msg);
	}
	
}