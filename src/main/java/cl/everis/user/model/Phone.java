package cl.everis.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="PHONE")
public class Phone implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9064899728436848901L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PHONE")
	private Long id;
	
	@Column(name = "NUMBER")
	private String number;

	@Column(name = "CITY_CODE")
	private String citycode;
	
	@Column(name = "CONTRY_CODE")
	private String contrycode;

	@Column(name = "ID_USER")
	@JsonIgnore
	private Long iduser;
	
}
