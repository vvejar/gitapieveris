package cl.everis.user.validator;

import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import cl.everis.user.exception.ServiceException;

@Component
public class ValidatorUser {

	private Logger logger = LogManager.getLogger(ValidatorUser.class);
	
	/**
	 * Valida formato del Email de usuario
	 * @param email
	 * @return boolean
	 * @throws ServiceException 
	 */
	public boolean validaMail(String email) throws ServiceException{
		try{
			Pattern pattern = Pattern.compile("^[a-zA-Z]{7}@[(a-z-A-z)]+\\.[a-zA-z]{2}$");

			if(!pattern.matcher(email!=null?email.trim():null).matches()) {
				return false;
			}
			
			return true;
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException("Imposible validar formato del Email ["+email+"]");
		}
	}

	/**
	 * Valida formato de la clave de usuario
	 * @param password
	 * @return boolean
	 * @throws ServiceException
	 */
	public boolean validaPassword(String password) throws ServiceException{
		try{
			Pattern pattern = Pattern.compile("^[A-Z][a-z]+\\d{2}$");
			
			if(!pattern.matcher(password!=null?password.trim():null).matches()) {
				return false;
			}
			
			return true;

		}catch(Exception e){
			logger.error(e);
			throw new ServiceException("Imposible validar formato del Password");
		}
	}
	
}
