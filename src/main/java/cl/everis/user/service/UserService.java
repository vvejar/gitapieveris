package cl.everis.user.service;

import java.util.List;

import cl.everis.user.exception.NoDataException;
import cl.everis.user.exception.ServiceException;
import cl.everis.user.model.User;

public interface UserService {
	
	/**
	 * Permite crear un usuario en base de datos
	 * @param user
	 * @return User
	 * @throws ServiceException
	 */
	public User createUser(User user) throws ServiceException;
	
	/**
	 * Permite recuperar el usuario por su Id
	 * @param id
	 * @return User
	 * @throws ServiceException
	 * @throws NoDataException 
	 */
	public User getUserId(Long id) throws ServiceException, NoDataException;

	/**
	 * Permite actualizar los datos del usuario
	 * @param user
	 * @return User
	 * @throws ServiceException
	 * @throws NoDataException 
	 */
	public User updateUser(User user) throws ServiceException, NoDataException;

	/**
	 * Permite eliminar un usuario por su Id
	 * @param id
	 * @return boolean
	 * @throws ServiceException
	 */
	public boolean deleteUser(Long id) throws ServiceException;
	
	/**
	 * Permite recuperar todos los usuarios del registro
	 * @return List<User>
	 * @throws ServiceException
	 * @throws NoDataException 
	 */
	public List<User> getUsers() throws ServiceException, NoDataException;
}
