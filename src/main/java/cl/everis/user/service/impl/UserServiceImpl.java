package cl.everis.user.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import cl.everis.user.exception.NoDataException;
import cl.everis.user.exception.ServiceException;
import cl.everis.user.model.User;
import cl.everis.user.repository.UserRepository;
import cl.everis.user.service.UserService;
import cl.everis.user.validator.ValidatorUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private final Logger logger = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ValidatorUser validatorUser;

	@Value("${msg.mail}")
	private String msgMail;

	@Override
	public User createUser(User user) throws ServiceException {
	
		try{

			validacionNew(user);
			user.setId(null);
			user.setName(Objects.toString(user.getName(), "").trim());
			user.setEmail(Objects.toString(user.getEmail(), "").trim());
			user.setPassword(Objects.toString(user.getPassword(), "").trim());
			user.setLast_login(new Timestamp(System.currentTimeMillis()));
			user.setToken(getToken(user));
			user.setIsactive(true);
			return userRepository.save(user);
			
		}catch(ServiceException e){
			logger.error(e);
			throw e;
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException("Problemas al crear Usuario");
		}
						
	}

	@Override
	public User getUserId(Long id) throws ServiceException, NoDataException {
		try{
			return userRepository.findById(id).get();
		}catch(NoSuchElementException e){
			logger.error(e);
			throw new NoDataException("Sin Datos");
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException(e.getMessage());
		}
	}
	
	@Override
	public User updateUser(User user) throws ServiceException , NoDataException {
		
		try{
			User userOld = getUserId(user.getId());
			validacionUpdate(user, userOld);
			user.setName(Objects.toString(user.getName(), "").trim());
			user.setEmail(Objects.toString(user.getEmail(), "").trim());
			user.setPassword(Objects.toString(user.getPassword(), "").trim());
			user.setLast_login(new Timestamp(System.currentTimeMillis()));
			user.setToken(getToken(user));
			return userRepository.save(user);

		}catch(NoDataException e){
			logger.error(e);
			throw e;
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException(e.getMessage());
		}
	}

	@Override
	public boolean deleteUser(Long id) throws ServiceException {
		
		try{
			userRepository.deleteById(id);
			return true;

		}catch(EmptyResultDataAccessException e){
			logger.error(e);
			throw new ServiceException("Sin Datos a eliminar");
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException(e.getMessage());
		}
	}
	
	@Override
	public List<User> getUsers() throws ServiceException, NoDataException {
		try{
			List<User> list = (List<User>) userRepository.findAll();
			
			if (list == null || list.isEmpty()){
				throw new NoDataException("Sin Datos");
			}
			
			return list;
		}catch(NoDataException e){
			logger.error(e);
			throw e;
		}catch(Exception e){
			logger.error(e);
			throw new ServiceException(e.getMessage());
		}
	}

	/**
	 * Aplica validaciones de datos al usuario nuevo
	 * @param user
	 * @throws ServiceException 
	 */
	private void validacionNew(User user) throws ServiceException{
	
		if(existeMailNew(user.getEmail())){
			throw new ServiceException(msgMail);
		}
		
		validacionDatos(user);
	}
	
	/**
	 * Aplica validaciones de datos al usuario actualizado
	 * @param user
	 * @param userOld 
	 * @throws ServiceException 
	 */
	private void validacionUpdate(User user, User userOld) throws ServiceException{
	
		if(existeMailUpdate(user.getEmail(), userOld)){
			throw new ServiceException(msgMail);
		}
		
		validacionDatos(user);
	}
	
	/**
	 * Aplica validaciones de datos al usuario
	 * @param user
	 * @throws ServiceException 
	 */
	private void validacionDatos(User user) throws ServiceException{
	
		if(!validatorUser.validaMail(user.getEmail())){
			throw new ServiceException("Formato del Email incorrecto [aaaaaaa@dominio.xx] - ["+user.getEmail()+"]");
		}
		if(!validatorUser.validaPassword(user.getPassword())){
			throw new ServiceException("Formato de Password incorrecto [Una Mayuscula, letras minúsculas, y dos números]");
		}
	}
	
	/**
	 * Valida si ya existe el Email en el registro de usuarios, para nuevo usuario
	 * @param email
	 * @return boolean
	 * @throws ServiceException
	 */
	private boolean existeMailNew(String email) throws ServiceException{
		try{
			List<?> list = userRepository.findByEmail(email!=null?email.trim():null);
			
			if (list != null && !list.isEmpty()){
				return true;
			}
			
			return false;

		}catch(Exception e){
			logger.error(e);
			throw new ServiceException("Imposible validar si existe Email en BD");
		}
	}
	
	/**
	 * Valida si ya existe el Email en el registro de usuarios, para usuario actualizado
	 * @param email
	 * @param userOld 
	 * @return boolean
	 * @throws ServiceException
	 */
	private boolean existeMailUpdate(String email, User userOld) throws ServiceException{
		try{
			List<User> list = userRepository.findByEmail(email!=null?email.trim():null);
			
			if (list != null && !list.isEmpty() && !userOld.getEmail().equalsIgnoreCase(list.get(0).getEmail())){
				return true;
			}
			
			return false;

		}catch(Exception e){
			logger.error(e);
			throw new ServiceException("Imposible validar si existe Email en BD");
		}
	}
	/**
	 * Genera token de acceso para el usuario
	 * @param user
	 * @return String
	 */
	private String getToken(User user) {
		
		Claims claims = Jwts.claims().setSubject(user.getName());
		 
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(user.getName())
				.signWith(SignatureAlgorithm.HS256, user.getPassword().getBytes())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setIssuer(user.getName())
				.setExpiration(new Date(System.currentTimeMillis() + 60*1000))
				.compact();
	}

}
