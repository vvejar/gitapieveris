
package cl.everis.user.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.everis.user.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	List<User> findByName(@Param("name") String name);
	
	List<User> findByEmail(@Param("email") String email);
	
}
