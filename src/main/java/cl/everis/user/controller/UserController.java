package cl.everis.user.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.everis.user.exception.NoDataException;
import cl.everis.user.model.User;
import cl.everis.user.service.UserService;
import cl.everis.user.vo.ResponseVO;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

	private final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;

	/**
	 * Permite crear un usuario
	 * @param user
	 * @return ResponseEntity<?>
	 */
	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody User user) {

		logger.info("--- INICIO createUser ---");
		
		try {

			return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseVO(e.getMessage()));
		}
	}
	
	/**
	 * Permite recuperar la lista de usuarios
	 * @return ResponseEntity<?>
	 */
	@GetMapping
	public ResponseEntity<?> getUsers() {
		
		logger.info("--- INICIO getUsers ---");
		
		try {

			return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
		
        } catch (NoDataException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ResponseVO(e.getMessage()));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseVO(e.getMessage()));
		}
	}
	
	/**
	 * Permite recuperar un usuario por su id
	 * @param id
	 * @return ResponseEntity<?>
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> getUserId(@PathVariable("id") Long id) {
		
		logger.info("--- INICIO getUserId ---");
		logger.info("--- id: "+id);
		
		try {

			return new ResponseEntity<>(userService.getUserId(id), HttpStatus.OK);
			
        } catch (NoDataException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ResponseVO(e.getMessage()));			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseVO(e.getMessage()));
		}
	}
	
	/**
	 * Permite actualizar los datos de un usuario
	 * @param user
	 * @return ResponseEntity<?>
	 */
	@PutMapping
	public ResponseEntity<?> updateUser(@RequestBody User user) {

		logger.info("--- INICIO updateUser ---");
		
		try {

			return new ResponseEntity<>(userService.updateUser(user), HttpStatus.OK);
			
        } catch (NoDataException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ResponseVO(e.getMessage()));		
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseVO(e.getMessage()));
		}
	}
	
	/**
	 * Permite eliminar un usuario por su id
	 * @param id
	 * @return ResponseEntity<?>
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
		
		logger.info("--- INICIO deleteUser ---");
		logger.info("--- id: "+id);
		
		try {

			return new ResponseEntity<>(new ResponseVO(String.valueOf(userService.deleteUser(id))), HttpStatus.OK);
					
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseVO(e.getMessage()));
		}
	}
}
