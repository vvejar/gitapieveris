package cl.everis.user.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVO implements Serializable {
    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3140073712233760548L;
    
    private String mensaje;

}