package cl.everis.user

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

import cl.everis.user.controller.UserController
import cl.everis.user.exception.NoDataException
import cl.everis.user.exception.ServiceException
import cl.everis.user.model.User
import cl.everis.user.service.UserService
import spock.lang.Specification

class UserControllertSpec extends Specification {

  def UserService service
  def UserController userController

  def setup() {
	this.service = Mock(UserService.class)
    this.userController = new UserController(service)
  }

  /**
   * Pruebas crear usuario
   */
  def "Prueba de crear usuario exitoso"() {
	  given: "crea un usuario"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.createUser(user)>> { user }
	  when: "crea nuevo usuario"
	  ResponseEntity result = this.userController.createUser(user)
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.CREATED
  }
  
  def "Prueba de crear usuario - error"() {
	  given: "crea un usuario error"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.createUser(user)>> { throw new ServiceException("prueba error creaUsuario() - Exception") }
	  when: "crea nuevo usuario error"
	  ResponseEntity result = this.userController.createUser(user)
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.EXPECTATION_FAILED
  }

  /**
   * Pruebas actualiza usuario
   */
  def "Prueba de actualiza usuario exitoso"() {
	  given: "actualiza un usuario"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.updateUser(user)>> { user }
	  when: "actualiza usuario"
	  ResponseEntity result = this.userController.updateUser(user)
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.OK
  }
  
  def "Prueba de actualiza usuario - error"() {
	  given: "actualiza un usuario error"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.updateUser(user)>> { throw new ServiceException("prueba error actualizaUsuario() - ServiceException") }
	  when: "actualiza usuario error"
	  ResponseEntity result = this.userController.updateUser(user)
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.EXPECTATION_FAILED
  }
  
  def "Prueba de actualiza usuario - sin datos "() {
	  given: "actualiza un usuario sin datos"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.updateUser(user)>> { throw new NoDataException("prueba error actualizaUsuario() - NoDataException") }
	  when: "actualiza usuario sin datos"
	  ResponseEntity result = this.userController.updateUser(user)
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.NO_CONTENT
  }
  
  /**
   * Pruebas recupera usuario
   */
  def "Prueba de recupera usuario exitoso"() {
	  given: "recupera un usuario"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUserId(user.getId())>> { user }
	  when: "recupera usuario"
	  ResponseEntity result = this.userController.getUserId(user.getId())
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.OK
  }
  
  def "Prueba de recupera usuario sin datos"() {
	  given: "recupera un usuario sin datos"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUserId(user.getId())>> { throw new NoDataException("prueba sin datos getUsuario() - NoDataException") }
	  when: "recupera usuario sin datos"
	  ResponseEntity result = this.userController.getUserId(user.getId())
	  then: "Respuesta OK"
	  println result
	  result.statusCode == HttpStatus.NO_CONTENT
  }
  
  def "Prueba de recupera usuario error"() {
	  given: "recupera un usuario error"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUserId(user.getId())>> { throw new ServiceException("prueba error getUsuario() - ServiceException") }
	  when: "recupera usuario error"
	  ResponseEntity result = this.userController.getUserId(user.getId())
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.EXPECTATION_FAILED
  }
  
  def "Prueba de recupera usuarios exitoso"() {
	  given: "recupera usuarios"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUsers()>> { [user] }
	  when: "recupera usuarios"
	  ResponseEntity result = this.userController.getUsers()
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.OK
  }
  
  def "Prueba de recupera usuarios sin datos"() {
	  given: "recupera usuarios sin datos"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUsers()>> { throw new NoDataException("prueba sin datos getUsuarios() - NoDataException") }
	  when: "recupera usuarios sin datos"
	  ResponseEntity result = this.userController.getUsers()
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.NO_CONTENT
  }
  
  def "Prueba de recupera usuarios error"() {
	  given: "recupera usuarios error"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.getUsers()>> { throw new ServiceException("prueba error getUsuarios() - ServiceException") }
	  when: "recupera usuarios error"
	  ResponseEntity result = this.userController.getUsers()
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.EXPECTATION_FAILED
  }
  
  /**
   * Pruebas elimina usuario
   */
  def "Prueba de elimina usuario exitoso"() {
	  given: "elimina un usuario"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.deleteUser(user.getId())>> { Boolean.TRUE }
	  when: "elimina usuario"
	  ResponseEntity result = this.userController.deleteUser(user.getId())
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.OK
  }
  
  def "Prueba de elimina usuario error"() {
	  given: "elimina usuario error"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.service.deleteUser(user.getId())>>  { throw new ServiceException("prueba error eliminaUsuario() - ServiceException") }
	  when: "elimina usuario error"
	  ResponseEntity result = this.userController.deleteUser(user.getId())
	  then: "Respuesta OK"
	  result.statusCode == HttpStatus.EXPECTATION_FAILED
  }
  
}
