package cl.everis.user

import cl.everis.user.controller.UserController
import cl.everis.user.exception.NoDataException
import cl.everis.user.exception.ServiceException
import cl.everis.user.model.Phone
import cl.everis.user.model.User
import cl.everis.user.repository.UserRepository
import cl.everis.user.service.UserService
import cl.everis.user.service.impl.UserServiceImpl
import cl.everis.user.validator.ValidatorUser
import java.sql.Timestamp
import org.apache.tomcat.jni.Time
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Subject
import spock.mock.DetachedMockFactory

class UserServiceSpec extends Specification {

  def UserService service
  def UserRepository repository
  def ValidatorUser validatorUser;

  def setup() {
	this.repository = Mock(UserRepository.class)
	this.validatorUser = new ValidatorUser();
    this.service = new UserServiceImpl(repository,	validatorUser, "El correo ya registrado")
  }

  /**
   * Pruebas crear usuario
   */
  def "Prueba de crear usuario exitoso"() {
	  given: "crea un usuario"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findByEmail(user.email)>> { new ArrayList()}
	  this.repository.save(user)>> { user }
	  when: "crea nuevo usuario"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  println result
	  result != null
  }
  
  def "Prueba de crear usuario error mail existe"() {
	  given: "crea un usuario error mail existe"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findByEmail(user.email)>> { [user] }
	  when: "crea nuevo usuario error mail existe"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de crear usuario error"() {
	  given: "crea un usuario error"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.save(user)>> { throw new Exception("prueba error createUser()") }
	  when: "crea nuevo usuario error"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }
  def "Prueba de crear usuario error mail"() {
	  given: "crea un usuario error mail"
	  User user = User.builder()
	  .email("johntes@domain.com2")
	  .password("Wtest12").build();
	  when: "crea usuario error mail"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de crear usuario error mail - nulo"() {
	  given: "crea un usuario error mail - nulo"
	  User user = User.builder()
	  .email(null)
	  .password("Wtest12").build();
	  when: "crea usuario error mail - nulo"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de crear usuario error password"() {
	  given: "crea un usuario error password"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("wtest12").build();
	  when: "crea usuario error password"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de crear usuario error password - nulo"() {
	  given: "crea un usuario error password - nulo"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password(null).build();
	  when: "crea usuario error password - nulo"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de crear usuario error mail - Exception"() {
	  given: "crea un usuario error mail - Exception"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findByEmail(user.email)>> { throw new Exception("prueba error findByEmail() - Exception") }
	  when: "crea usuario error mail - Exception"
	  User result = this.service.createUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  /**
   * Pruebas actualizar usuario
   */
  def "Prueba de actualizar usuario exitoso"() {
	  given: "actualiza un usuario"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findById(user.getId()) >> { new Optional<User>(user) }
	  this.repository.save(user)>> { user }
	  when: "actualiza usuario"
	  User result = this.service.updateUser(user)
	  then: "Respuesta OK"
	  println result
	  result != null
  }
  
  def "Prueba de actualizar usuario no existente"() {
	  given: "actualiza un usuario no existente"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findById(user.getId()) >> { throw new NoSuchElementException("prueba error updateUser() - sin datos") }
	  this.repository.save(user)>> { user }
	  when: "actualiza usuario no existente"
	  User result = this.service.updateUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }
  
  def "Prueba de actualizar usuario error"() {
	  given: "actualiza un usuario error"
	  User user = User.builder()
	  .id(1)
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findById(user.getId()) >> { throw new Exception("prueba error updateUser() - Exception") }
	  this.repository.save(user)>> { user }
	  when: "actualiza usuario error"
	  User result = this.service.updateUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }
  
  def "Prueba de actualizar usuario error mail - existe"() {
	  given: "actualiza un usuario error mail - existe"
	  User userOld = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  User userNew = User.builder()
	  .email("johntes1@domain.co")
	  .password("Wtest12").build();
	  this.repository.findById(userNew.getId()) >> { new Optional<User>(userOld) }
	  this.repository.findByEmail(userNew.email)>> { [userNew] }
	  when: "actualiza usuario error mail - existe"
	  User result = this.service.updateUser(userNew)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex
  }
  
  def "Prueba de actualizar usuario error mail - Exception"() {
	  given: "actualiza un usuario error mail - Exception"
	  User user = User.builder()
	  .email("johntes@domain.co")
	  .password("Wtest12").build();
	  this.repository.findById(user.getId()) >> { new Optional<User>(user) }
	  this.repository.findByEmail(user.email)>> { throw new ServiceException("prueba error findByEmail() - Exception") }
	  when: "actualiza usuario error mail - Exception"
	  User result = this.service.updateUser(user)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex
  }
  
  /**
   * Pruebas obtener usuarios
   */
  def "Prueba de obtener usuarios exitoso"() {
      given: "lista de usuarios"
      this.repository.findAll()>> { [new User()] }
      when: "obtener usuarios"
      List<User> result = this.service.getUsers()
      then: "Respuesta OK"
      !result.isEmpty()
  }

  def "Prueba de obtener usuarios lista nula"() {
	  given: "lista de usuarios nula"
	  this.repository.findAll()>> { null }
	  when: "obtener usuarios nula"
	  List<User> result = this.service.getUsers()
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
      !ex.message.isEmpty()
  }
  
  def "Prueba de obtener usuarios lista vacia"() {
	  given: "lista de usuarios vacia"
	  this.repository.findAll()>> { new ArrayList() }
	  when: "obtener usuarios vacia"
	  List<User> result = this.service.getUsers()
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
	  !ex.message.isEmpty()
  }
  
  def "Prueba de obtener usuarios error"() {
	  given: "lista de usuarios error"
	  this.repository.findAll()>> { throw new Exception("prueba error getUsers()") }
	  when: "obtener usuarios error"
	  List<User> result = this.service.getUsers()
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }

  /**
   * Pruebas eliminar usuario
   */
  def "Prueba de eliminar usuario exitoso"() {
	  given: "usuario a eliminar"
	  this.repository.deleteById(1)>> { true }
	  when: "eliminar usuario"
	  boolean result = this.service.deleteUser(1)
	  then: "Respuesta OK"
	  result
  }

  def "Prueba de eliminar usuario sin datos"() {
	  given: "usuario a eliminar sin datos"
	  this.repository.deleteById(1)>> { throw new EmptyResultDataAccessException("prueba error deleteUser() - sin datos", 1) }
	  when: "eliminar usuario sin datos"
	  boolean result = this.service.deleteUser(1)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }
  
  def "Prueba de eliminar usuario error"() {
	  given: "usuario a eliminar error"
	  this.repository.deleteById(1)>> { throw new ServiceException("prueba error deleteUser() - Exception") }
	  when: "eliminar usuario error"
	  boolean result = this.service.deleteUser(1)
	  then: "Respuesta OK"
	  def ex = thrown(Exception)
	  println ex.message
  }
}
