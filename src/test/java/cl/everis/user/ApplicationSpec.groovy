package cl.everis.user

import org.springframework.boot.SpringApplication

import spock.lang.Specification

class ApplicationSpec extends Specification {
	
	def "Prueba de creacion - vacio"() {
		when:
		Application application = new Application();
		then:
		application != null
	}

//	def "Prueba de creacion - main"() {
//		given:
//		String[] args = ["one", "two"];
//		SpringApplication.run(Application.class, args) >> { throw new Exception("prueba error main()") }
//		when:
//		Application.main(args)
//		then:
//		thrown(Exception)
//	}
}
