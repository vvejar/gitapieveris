package cl.everis.user

import cl.everis.user.model.User
import cl.everis.user.model.Phone
import java.sql.Timestamp
import spock.lang.Specification

class UserSpec extends Specification {

	/**
	 * Pruebas crear usuario
	 */
	def "Prueba objeto User"() {
		given: "crea un usuario"
		def phone = Phone.builder().citycode("2").contrycode("56").id(1).iduser(1).number("253625").build()
		when: "crea nuevo usuario"
		phone.getCitycode();
		phone.getContrycode()
		phone.getId()
		phone.getIduser()
		phone.getNumber()
		
		phone.setCitycode("2");
		phone.setContrycode("56")
		phone.setId(1)
		phone.setIduser(1)
		phone.setNumber("253625")
		
		User result = User.builder()
		.email("johntes@domain.co")
		.password("Wtest12")
		.created(new Timestamp(System.currentTimeMillis()))
		.id(1)
		.isactive(false)
		.last_login(new Timestamp(System.currentTimeMillis()))
		.modified()
		.name("Test")
		.phones([phone, new Phone()])
		.token("token").build();
		
		result.setCreated(new Timestamp(System.currentTimeMillis()));
		result.setModified(new Timestamp(System.currentTimeMillis()));
		result.setPhones([phone, new Phone()]);
		
		then: "Respuesta OK"
		println result
		println result.builder().toString()
		
		println phone.builder().toString()
		println phone.equals(phone)
		println phone.equals(result)
		println phone.equals(null)
		println phone.canEqual(phone)
		println phone.hashCode()
		
		println result.equals(null)
		println result.canEqual(phone)
		println result.hashCode()
		result != null
	}
}
