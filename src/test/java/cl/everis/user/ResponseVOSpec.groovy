package cl.everis.user

import cl.everis.user.vo.ResponseVO
import spock.lang.Specification

class ResponseVOSpec extends Specification {

	def "Prueba de creacion - vacio"() {
		when:
		ResponseVO responseVO = new ResponseVO()
		then:
		responseVO.getMensaje() == null
	}
	
	def "Prueba de creacion - mensaje"() {
		when:
		ResponseVO responseVO = new ResponseVO("mensaje")
		then:
		!responseVO.getMensaje().isEmpty()
	}
	
	def "Prueba de creacion - propiedades"() {
		when:
		ResponseVO responseVO = new ResponseVO();
		responseVO.setMensaje("mensaje")	
		println responseVO.getMensaje();
		println responseVO.toString();
		println responseVO.hashCode();
		println responseVO.equals(null);
		println responseVO.equals(responseVO);
		println responseVO.canEqual(responseVO);
		then:
		!responseVO.getMensaje().isEmpty()
	}
}
